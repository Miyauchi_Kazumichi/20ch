<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome 20ch</title>
<script type="text/javascript">
		function gate() {
			UserInput = prompt("パスワードを入力して下さい:","");
			if ( UserInput == "pass1" ){
				location.href= "http://localhost:8080/20ch/management";
				return true;
			}else{
					alert( "パスワードが違います!" );
					return false;
			}
		}
</script>
</head>
<body>
	<h1>${chMessage}</h1>
	<a href="text/insert/input">新規投稿</a><br/>

	<input type = "submit" value = "管理者" onClick = "return gate();">

	<h3>${errorMessage}</h3>
	<br />
	<form:form modelAttribute="commentForm">
		<div>
			<h4><form:errors path="*" /></h4>
		</div>
	</form:form>
	<c:forEach items="${messages}" var="message">
		<p>
			＜投稿内容＞<br />

		</p>
		<c:if test = "${message.isDeleted == 0 }">
			<c:out value="${message.text}"></c:out>

			<c:out value="${message.createdDate}"></c:out>
			<c:forEach items="${ comments }" var="comment">
					<c:if test="${message.id == comment.messageId }">
						<p>
							＜コメント＞<br />
						</p>
							<c:if test = "${comment.isDeleted == 0 }">
								<c:out value="${comment.text}"></c:out>
								<c:out value="${comment.createdDate}"></c:out>
								<br />
							</c:if>
					<c:if test = "${comment.isDeleted == 1 }">
						<c:out value= "管理者によりコメントは削除されました"></c:out>
					</c:if>
					</c:if>

				</c:forEach>

				<form:form modelAttribute="commentForm">
					<textarea name="text" id="text" cols="50" rows="5" class="text-box"></textarea>
					<input name="messageId" value="${message.id}" id="messageId" type=hidden><br/>
					<input type="submit" value="コメント" onclick="return checkComment();">
				</form:form>
		</c:if>

		<c:if test = "${message.isDeleted == 1 }">
			<c:out value= "管理者によりメッセージは削除されました"></c:out>
		</c:if>




	</c:forEach>

	<script type="text/javascript">
			$(function() {
				$('[type="submit"]').click(function() {
					$(this).prop('disabled', true);
					$(this).closest('form').submit();
				});
			});
		</script>

		<script type="text/javascript">
		    function checkComment(){
			    var comment = document.getElementById('commentText').value;
			    // 140文文字以下判定
			    if(comment.length > 140){
			        window.alert('140字以下で文字を入力してください');
			        return false;
			    }
			    // 空文字判定
			    else if(!comment || !comment.match(/\S/g)){
			    	window.alert('文字を入力してください');
			        return false;
			    }
			    return true;
			}
		</script>

</body>
</html>
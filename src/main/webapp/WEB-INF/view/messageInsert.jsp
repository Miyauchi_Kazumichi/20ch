<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
	<head>
		<meta charset="utf-8">
		<title>Welcome 30ch</title>
	</head>
	<body>
	    <h1>${chMessage}</h1>
	    <h3>${message}</h3>
	    <a href = "http://localhost:8080/20ch/home">戻る</a>
	    <form:form modelAttribute="messageForm" >
	   		<div><form:errors path="*"  /></div>
	   		<textarea name="text" id = "text" cols="100" rows="5" class="text-box" ></textarea>
	        <br><input type="submit" value = "投稿する" name = "message">
	    </form:form>

	    <form:form modelAttribute = "pictureForm" enctype = "multipart/form-data">
			ファイルを選択： <input type="file" name ="image" accept = ".jpg"/><br />
	        <input type="submit" value = "画像を投稿する">
	    </form:form>
	    <script type="text/javascript">
			$(function() {
				$('[type="submit"]').click(function() {
					$(this).prop('disabled', true);
					$(this).closest('form').submit();
				});
			});
		</script>
	</body>
</html>
package jp.co.ch.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.ch.dto.CommentDto;
import jp.co.ch.dto.MessageDto;
import jp.co.ch.form.CommentForm;
import jp.co.ch.form.MessageForm;
import jp.co.ch.form.PictureForm;
import jp.co.ch.service.ChannelService;

@Controller
public class ChannelController {

	@Autowired
	private ChannelService channelService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String textAll(Model model) {
		List<MessageDto> messages = channelService.getTextAll();
		List<CommentDto> comments = channelService.getCommentAll();

		model.addAttribute("chMessage", "20ch");
		model.addAttribute("messages", messages);
		model.addAttribute("comments", comments);
		return "home";
		//ここでの戻り値は表示したい対応したjspの名前
	}

	@RequestMapping(value = "/text/insert/input", method = RequestMethod.GET)
	public String messageInsert(Model model) {
		//MessageForm form = new MessageForm();
		//model.addAttribute("messageForm", form);
		model.addAttribute("chMessage", "投稿画面");
		return "messageInsert";
	}

	@RequestMapping(value = "/text/insert/input", params = "message",  method = RequestMethod.POST)
	public String messageInsert(@Valid @ModelAttribute MessageForm form, BindingResult result, Model model){
		if (result.hasErrors()) {
			model.addAttribute("title", "エラー");
			model.addAttribute("chMessage", "20ch");
			model.addAttribute("message", "以下のエラーを解消してください");
			return "messageInsert";
		} else {
			int count = channelService.insertMessage(form.getText());

			Logger.getLogger(ChannelController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		}
		return "redirect:/home";
	}

	@RequestMapping(value = "/text/insert/input", method = RequestMethod.POST)

	public String messageInsert(@ModelAttribute PictureForm form, Model model) throws Exception {
		StringBuffer data = new StringBuffer();
        String base64;

			base64 = new String(Base64.encodeBase64(form.getImage().getBytes()),"ASCII");
			data.append("data:image/jpeg;base64,");
	        data.append(base64);


		channelService.insertPicture(base64);
        model.addAttribute("base64image",data.toString());

        return "redirect:/home";
	}

	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String commentInsert(@Valid @ModelAttribute CommentForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			List<MessageDto> messages = channelService.getTextAll();
			List<CommentDto> comments = channelService.getCommentAll();
			model.addAttribute("title", "エラー");
			model.addAttribute("chMessage", "20ch");
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			model.addAttribute("messages", messages);
			model.addAttribute("comments", comments);
			return "home";
		} else {
			int count = channelService.insertComment(form);
			model.addAttribute("commentForm", form);
			Logger.getLogger(ChannelController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		}
		return "redirect:/home";
	}

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public String management(Model model) {
		List<MessageDto> messages = channelService.getTextAll();
		List<CommentDto> comments = channelService.getCommentAll();

		model.addAttribute("chMessage", "管理者用ページ");
		model.addAttribute("messages", messages);
		model.addAttribute("comments", comments);
		return "management";
	}

	@RequestMapping(value = "/management", method = RequestMethod.POST)
	public String management(@ModelAttribute MessageForm form, Model model) {

		channelService.updateIsDeleted(form);

		return "redirect:management";
	}

	@RequestMapping(value = "/management", params = "deleteComment", method = RequestMethod.POST)
	public String management(@ModelAttribute CommentForm form, Model model) {

		channelService.updateCommentIsDeleted(form);

		return "redirect:management";
	}


}
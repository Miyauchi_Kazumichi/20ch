package jp.co.ch.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class MessageDto {

	private Integer id;
	private String text;
	private Date created_date;
	private Date updated_date;
	private Integer isDeleted;
	private MultipartFile image;


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}
	public String getText() {
	     return text;
	}
	public void setText(String text) {
	     this.text = text;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getupdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

}

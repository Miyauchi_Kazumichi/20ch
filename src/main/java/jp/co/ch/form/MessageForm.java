package jp.co.ch.form;

import java.util.Date;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

public class MessageForm {
	private Integer id;

	@NotBlank(message = "空白の投稿はできません")
	@Length(max = 140, message = "投稿は140字以内までです")
	private String text;

	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date created_date;
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date updated_date;

	private Integer isDeleted;
	private MultipartFile image;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getupdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }
}

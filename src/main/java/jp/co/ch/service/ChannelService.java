package jp.co.ch.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.ch.dto.CommentDto;
import jp.co.ch.dto.MessageDto;
import jp.co.ch.entity.Comment;
import jp.co.ch.entity.Message;
import jp.co.ch.form.CommentForm;
import jp.co.ch.form.MessageForm;
import jp.co.ch.mapper.ChannelMapper;

@Service
public class ChannelService {

	@Autowired
	private ChannelMapper channelMapper;

	public List<MessageDto> getTextAll() {
		List<Message> textList = channelMapper.getTextAll();
		List<MessageDto> resultList = convertToDto(textList);
		return resultList;
	}

	public List<MessageDto> convertToDto(List<Message> textList) {
		List<MessageDto> resultList = new LinkedList<>();
		for (Message entity : textList) {
			MessageDto dto = new MessageDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public List<CommentDto> getCommentAll() {
		List<Comment> commentList = channelMapper.getCommentAll();
		List<CommentDto> resultCommentList = convertCommentToDto(commentList);
		return resultCommentList;
	}

	public List<CommentDto> convertCommentToDto(List<Comment> commentList) {
		List<CommentDto> resultCommentList = new LinkedList<>();
		for (Comment entity : commentList) {
			CommentDto dto = new CommentDto();
			BeanUtils.copyProperties(entity, dto);
			resultCommentList.add(dto);
		}
		return resultCommentList;
	}

	public int insertMessage(String text) {
		int count = channelMapper.insertMessage(text);
		return count;
	}

	public int insertComment(CommentForm form) {
		int count = channelMapper.insertComment(form);
		channelMapper.updateTime(form);
		return count;
	}

	public void updateIsDeleted(MessageForm form) {

		channelMapper.updateIsDeleted(form);

	}

	public void updateCommentIsDeleted(CommentForm form) {

		channelMapper.updateCommentIsDeleted(form);

	}

	public void insertPicture(String base64) {

		channelMapper.insertPicture(base64);
	}


}


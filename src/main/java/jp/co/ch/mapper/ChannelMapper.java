package jp.co.ch.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import jp.co.ch.entity.Comment;
import jp.co.ch.entity.Message;
import jp.co.ch.form.CommentForm;
import jp.co.ch.form.MessageForm;

@Component
public interface ChannelMapper {

	List<Message> getTextAll();

	List<Comment> getCommentAll();

	int insertMessage(String text);

	int insertComment(CommentForm form);

	void updateIsDeleted(MessageForm form);

	void updateCommentIsDeleted(CommentForm form);

	void updateTime(CommentForm form);

	void insertPicture(String base64);



}
